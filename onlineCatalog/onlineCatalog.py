import cherrypy
import time 
import json
import threading
import os
class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message

class OnlineCatalog():
	exposed = True
	def __init__(self, catalogID, broker, port, usersFilePath):
		self.catalogID = catalogID
		self.deviceList = []
		self.usersFilePath = usersFilePath
		self.menuGET = {'get userID': self.getUser, 'get deviceID': self.getDevice, 'get broker': self.getBroker}
		self.menuPUTPOST = {'add userID': self.addUser, 'add deviceID': self.addDevice }
		self.broker = { 'broker': broker, 'port': port }
		try:
			with open(usersFilePath, 'r') as source:
				self.usersList = json.load(source)['usersList']
				source.close()
		except FileNotFoundError:
			with open(usersFilePath, 'w') as source:
				json.dump({'usersList': [] }, source, indent = 4)
				source.close()
			self.usersList = []	
	def GET (self, *uri, **param):
		res = list(uri)
		command = ' '.join(uri)
		try:
			output = self.menuGET[command](list(param.values())[0])
		except KeyError:
			output = 'Wrong request'
		except IndexError:
			output = self.menuGET[command]()
		return output 
	def POST (self, *uri, **param):
		res = list(uri)
		command = ' '.join(uri)
		payload = json.loads(cherrypy.request.body.read())
		try:
			output = self.menuPUTPOST[command](payload, True)
		except KeyError:
			output = 'Wrong request'
		except CustomError as error:
			output = error.message
		return output

	def PUT (self, *uri, **param):
		res = list(uri)
		command = ' '.join(uri)
		payload = json.loads(cherrypy.request.body.read())
		try:
			output = self.menuPUTPOST[command](payload, False)
		except KeyError:
			output = 'Wrong request'
		return output 
	
	def getBroker(self):
		return json.dumps(self.broker, indent = 4)
	
	def getUser(self, userID = None):
		found = 'UserID not found'
		if userID != None:
			for i in range(len(self.usersList)):
				if self.usersList[i]['userID'] == userID:
					found = json.dumps(self.usersList[i], indent = 4)
		else:
			found = json.dumps(self.usersList, indent = 4)
		return found
	def getDevice(self, deviceID = None):
		found = 'deviceID not found'
		if deviceID != None:
			for i in range(len(self.deviceList)):
				if self.deviceList[i]['deviceID'] == deviceID:
					found = json.dumps(self.deviceList[i], indent = 4)
		else:
			found = json.dumps(self.deviceList, indent = 4)
		return found

	def addUser(self, *data):
		userData = data[0]
		isPOST = data[1]
		output = 'User added' 
		for i in range(len(self.usersList)):
			if self.usersList[i]['userID'] == userData['userID']:
				if isPOST:
					raise CustomError('userIs is not available')
				else:
					self.usersList.pop(i)
					output = 'User found and updated'
			
		self.usersList.append(userData)
		with open(self.usersFilePath, 'w') as source:
			json.dump({ "usersList": self.usersList }, source)
			source.close()
		return output 
	
	def addDevice(self, *data):
		deviceData = data[0]
		isPOST = data[1]
		deviceData['insert-timestamp'] = time.time()
		output = 'Device added'
		for i in range(len(self.deviceList)):
			if self.deviceList[i]['deviceID'] == deviceData['deviceID']:
				if isPOST:
					raise CustomError('DeviceID is not available')
				else:
					self.deviceList.pop(i)
					output = 'Device found and updated'
		self.deviceList.append(deviceData)
		return output
	





class refreshThread(threading.Thread):
	def __init__(self, threadID):
		threading.Thread.__init__(self)
		self.threadID = threadID
	def run(self):
		while keepAlive:
			time.sleep(20)
			
			value = len(catalog.deviceList)
			print(f'{value}')
			for i in range(len(catalog.deviceList)):
				if time.time()-catalog.deviceList[i]['insert-timestamp'] > 20:
					catalog.deviceList.pop(i)
					break
			print(f'{json.dumps(catalog.deviceList, indent = 4)}')








if __name__=='__main__':
	print(f'{os.getpid()}')
	userData = 'users.json'
	confData = 'conf.json'
	keepAlive = True
	with open(confData, 'r') as configuration:
		config = json.load(configuration)
		configuration.close()
	catalog = OnlineCatalog(config['broker'], config['port'], 'catalog1', userData)
	thread1 = refreshThread('thread1')
	conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tool.session.on': True 
		}
	}
	cherrypy.tree.mount(catalog,'/', conf)
	cherrypy.engine.start()
	thread1.start()
	cherrypy.engine.block()
	keepAlive = False 