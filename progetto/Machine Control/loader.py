import os

def start(confFilesPath):
    baseCommand = 'gnome-terminal '
    files = os.listdir(confFilesPath)
    print(files)
    elemList = [item.split('-')[1].split('.')[0] for item in files if item[:4] == 'conf'and  item[-4:] == '.txt']
    
    finalString = baseCommand
    for elem in elemList:
        finalString += f'  --tab --title="{elem}" --command "python machineDeviceConnectorMOD.py conf-{elem}.txt"' 
    os.system(finalString)



if __name__ == '__main__':

    
    confFilesPath = "/home/ale/Documenti/smartgym/progetto/Machine Control/confFiles"
 
    start(confFilesPath)

  