# This Python file uses the following encoding: utf-8
from hashlib import new
import types

from myMQTT import *
import os, threading, requests
from pathlib import Path
import sys
import datetime,socket
import json
import time
import cherrypy

from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtCore import QObject, Slot, Signal, QTimer, QUrl

s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.connect(("8.8.8.8",80))
localIp = s.getsockname()[0]
socketPort = 50095
url_srvCat = 'http://192.168.1.9:8080/'

def scheduledConverter(threshold,schedDict):
    newDict = {}

    for elem in schedDict:
        tmp = {}
        hourString = elem.split(' ')
        if len(hourString) == 3:
            date,start,end = hourString
            startString = f"{date} {start}"
            endString = f"{date} {end}"
            tmp[startString] = schedDict[elem].copy()
            tmp[startString]['Start'] = True
            tmp[endString] = schedDict[elem].copy()
            tmp[endString]['Start'] = False
            for type in tmp[endString].keys():
                if tmp[endString][type] == True: tmp[endString][type] = False
                if type == 'Threshold': tmp[endString][type] = threshold
        else:
            date,start = hourString
            startString = f"{date} {start}"
            tmp[startString] = schedDict[elem].copy()
        newDict.update(tmp)
    return newDict

def colConv(num):
    num = int(num)
    if num == 0: return 'green'
    if num == 1: return 'orange'
    if num == 2: return 'grey'
    if num == 3: return 'red'
    return

class refreshThread(threading.Thread):
    def __init__(self, threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
    def run(self):
        while True:
            time.sleep(5)
            url_OD = f'http://{localIp}:{socket_port}/'
            srvId = 'Owner dashboard'
            registration_payload = dict(serviceID=srvId,url=url_OD)
            try:    
                r = requests.put(url_srvCat + "register serviceID", data=json.dumps(registration_payload))
            except:
                print('Connecton Error!')

class MainWindow(QObject,threading.Thread):
    def __init__(self):
        QObject.__init__(self)
        threading.Thread.__init__(self)

        self.fileName = 'data.json'
        self.srvCat = 'http://192.168.1.9:8080/'
        self.context = {'gym':None,'room':None,'menu':'home'}
        self.updateFlag = {'gyms':False,'rooms':False,'devices':False,'status':False}
        self.updateList = []
        self.displayLock = threading.Lock()
        self.dataLock = threading.Lock()
        self.totalSet = set()
        self.closingEvent = threading.Event()
        self.SRMConfig = {'lastUpdate':'','gymList':[]}
        self.SRMconfigurations = {}

        while True:
            try: 
                mqttConfig = requests.get(self.srvCat+"get broker").json()
                break
            except:
                print('Service catalog onnection error ')
                time.sleep(5)

        self.mqttClient = MyMQTT('mainListener', mqttConfig['broker'],mqttConfig['port'],self)
        self.mqttClient.start()
        self.dataStartup()

    # Ale signals
    updElem = Signal('QVariant')
    gymSig = Signal(list)
    roomSig = Signal(list)
    devSig = Signal(list)
    devListSig = Signal(int,list)
    adjSig = Signal(int,list)
    connStatus = Signal('QVariant')
    userSig = Signal('QVariant')

    signalAle = Signal(int)
    removeConfSig = Signal(int)
    loadModelData = Signal(list)
    populateSRMModel = Signal('QVariant')
    userList = Signal(int,list)

    # Signal Set Data
    printTime = Signal(str)
    
    def start_engine(self):
        self.engine = QQmlApplicationEngine()
        self.start()

    def subscribeTo(self,topic):
        self.mqttClient.mySubscribe(topic)

    # Set Timer Function

    def setTime(self):
        now = datetime.datetime.now()
        formatDate = now.strftime("Now is %H:%M:%S of %Y/%m/%d")
        #print(formatDate)
        self.printTime.emit(formatDate)
        
    ################# ALE'S BACKEND ###############
    def run(self):
        self.closingEvent.set()
        while self.closingEvent.is_set():
            self.updateData()
            time.sleep(5)        
        
    ##### FRONT-END METHODS #####
    @Slot(str,str)
    def loadData(self,selGym,selRoom):
        try:
            typeSet = set()
            typeSet.update([self.dataStructure[selGym][selRoom][device]['type'] for device in self.dataStructure[selGym][selRoom]])
            
            returnData = [{'name':str(elem)} for elem in sorted(typeSet)]
            self.loadModelData.emit(returnData)
        except:
            print('Something wrong in loadData')

    @Slot()
    def sendConf(self):
        try:
            SRMurl = requests.get(self.srvCat+'get/serviceID?serviceID=Specific Room Management').json()['url']
            payload = json.dumps(self.SRMConfig)
            print(payload)
            response = requests.post(SRMurl,data = payload)
            self.SRMConfig = {'lastUpdate':'','gymList':[]}
            self.SRMconfigurations = {}
        except:
            print('Connection error')
            return
        

    @Slot(str)
    def removeConf(self,confName):
        ######SE LA RIMOZIONE AVVIENE CON SUCCESSO FAI EMIT 0 ALTRIMENTI EMIT 1
        #print(confName)
        #self.removeConfSig.emit(0) #Se tutto ok
        #self.removeConfSig.emit(1) #Se qualcosa è andato storto
        pass

    @Slot(str)
    def receiveConfElem(self,newData):
        newData = json.loads(newData)
        print(newData)
        gymName = newData['gymName']
        roomName = newData['roomName']
        totalThreshold = newData['totalThreshold']
        actionDict = newData['actionDict']
        confName = newData['confName']
        scheduledDict = scheduledConverter(totalThreshold,newData['scheduledDict'])
        conf = f"{gymName}&{roomName}&{confName}"

        if conf in list(self.SRMconfigurations): 
            self.signalAle.emit(1)
            return

        gymIndex = next((index for (index, data) in enumerate(self.SRMConfig['gymList']) if data["name"] == gymName),None)
        if gymIndex == None:
            self.SRMconfigurations[conf] = dict(gymName=gymName,roomName=roomName,totalThreshold=totalThreshold,actionDict=actionDict,scheduledDict=scheduledDict)
            tmp = dict(name=roomName,totalThreshold=totalThreshold,actionDict=actionDict,scheduledDict=scheduledDict)
            self.SRMConfig['gymList'].append(dict(name=gymName,roomList=[tmp]))
            self.signalAle.emit(0)

        else:
            roomIndex = next((index for (index, data) in enumerate(self.SRMConfig['gymList'][gymIndex]['roomList']) if data["name"] == roomName),None)
            if roomIndex == None:
                self.SRMconfigurations[conf] = dict(gymName=gymName,roomName=roomName,totalThreshold=totalThreshold,actionDict=actionDict,scheduledDict=scheduledDict)
                self.SRMConfig['gymList'][gymIndex]['roomList'].append(dict(name=roomName,totalThreshold=totalThreshold,actionDict=actionDict,scheduledDict=scheduledDict))                 
                self.signalAle.emit(0)
            else:
                self.SRMconfigurations[conf] = dict(gymName=gymName,roomName=roomName,totalThreshold=totalThreshold,actionDict=actionDict,scheduledDict=scheduledDict)
                self.SRMConfig['gymList'][gymIndex]['roomList'][roomIndex]['actionDict'] = actionDict
                self.SRMConfig['gymList'][gymIndex]['roomList'][roomIndex]['scheduledDict'].update(scheduledDict)    
                self.signalAle.emit(0)          

        dispDict = dict(confName=confName,gymNameView=gymName,roomNameView=roomName)
        dispDict['actionDictView'] = json.dumps(actionDict,indent=4)
        dispDict['scheduledDictView'] = json.dumps(scheduledDict,indent=4)
        self.populateSRMModel.emit(dispDict)

    @Slot(str,str,str,str)
    def pairList(self,gym,room,M1,M2):
        if gym == 'None':
            self.gymSig.emit([{'name':gym,'status':colConv(self.status[gym]['status'])}for gym in self.dataStructure])
        elif room == 'None':
            self.roomSig.emit([{'name':room,'status':colConv(self.status[gym][room]['status'])}for room in self.dataStructure[gym]])
        elif M1 == 'None': 
            self.devListSig.emit(1,[{'name':dev,'status':'green'}for dev,val in self.dataStructure[gym][room].items() if val['isMachine'] == True])
        elif M2 == 'None':
            self.devListSig.emit(2,[{'name':dev,'status':'green'}for dev,val in self.dataStructure[gym][room].items() if dev != M1 and val['isMachine'] == True])
        else:
            self.updateMachine(gym,room,M1,M2)

    @Slot(str,str,str)
    def getAdjacency(self,gym,room,M1):
        if gym == 'None':
            self.adjSig.emit(1,[{'name':gym,'status':colConv(self.status[gym]['status'])}for gym in self.dataStructure])
        elif room == 'None':
            self.adjSig.emit(2,[{'name':room,'status':colConv(self.status[gym][room]['status'])}for room in self.dataStructure[gym]])
        elif M1 == 'None': 
            self.adjSig.emit(3,[{'name':dev,'status':'green'}for dev,val in self.dataStructure[gym][room].items() if val['isMachine'] == True])
        else:
            m = self.adj.get(M1,None)
            if m == None:
                self.adjSig.emit(4,[])
            else:    
                self.adjSig.emit(4,[{'name':elem,'status':'green'}for elem in self.adj[M1]])

    @Slot(str,str)
    def retrieveData(self,gym,room):
        if room == 'None':
            self.context['room'] = None
            self.updateFlag['rooms'] = True
        else:
            self.context['room'] = room
            self.updateFlag['devices'] = True 
        self.context['gym'] = gym
        self.display() 

    @Slot(str)
    def updateContext(self,selectedWindow):
        if selectedWindow == 'home':
            self.context['gym'] = None
            self.context['room'] = None
            self.gymSig.emit([{'name':gym,'status':colConv(self.status[gym]['status'])}for gym in self.dataStructure])
            self.context['menu'] = 'home'
        else:
            self.context['menu'] = selectedWindow
            self.context['gym'] = None
            self.context['room'] = None
   
    @Slot('QVariant')
    def addUser(self,data):
        data = json.loads(data)
        try:
            uDatabaseUrl = requests.get(self.srvCat+'get/serviceID?serviceID=user_database').json()['url']
        except:
            self.userList.emit(0,[{'error':'User database service disconnected!','title':"Connection error"}])
            return

        if data.get('userID',None) == None:
            response = requests.get(uDatabaseUrl+'get userID')
            if response.status_code == 200: self.userList.emit(2,response.json())
            else: self.userList.emit(0,[{'error':response.text,'title':'Connection error'}])
            return
        
        try:
            response = requests.put(uDatabaseUrl + "add userID", data=json.dumps(data))
            if response.text != 'User added': self.userList.emit(0,[{'error':response.text,'title':'User Database error'}])
            else: self.userList.emit(1,[data])
        except:
            self.userList.emit(0,[{'error':'Connection error!','title':'Connection error'}])

    @Slot()
    def getUsers(self):
        try:
            uDatabaseUrl = requests.get(self.srvCat+'get/serviceID?serviceID=user_database').json()['url']
        except:
            self.connStatus.emit({"status":"red","description":"Connection Error"})
            return
        response = requests.get(uDatabaseUrl + "get userID").json()
        self.userSig.emit(response)
   
   ##### BACK-END METHODS #####
    def notify(self,topic,msg):
        status = int(msg)
        gym,room,devID = topic.split('/')[1:3] + [topic.split('/')[-2]]
        self.updateFlag['status'] = True
        self.dataStructure[gym][room][devID]['status'] = status
        self.updateList.append(f"{gym}/{room}/{devID}/{status}")
        self.display()

    def updateMachine(self,gym,room,M1,M2):
        if self.adj.get(M1,None) == None and self.adj.get(M2,None) == None: #Due nuove macchine
            self.adj[M1] = list()
            self.adj[M2] = list()
            self.adj[M1].append(M2)
            self.adj[M2].append(M1)
        elif self.adj.get(M1,None) == None: #M1 non presente, M2 presente
            self.adj[M1] = list()
            self.adj[M1].append(M2)
            self.adj[M2].append(M1)
        elif self.adj.get(M2,None) == None: #M2 non presente, M1 presente
            self.adj[M2] = list()
            self.adj[M1].append(M2)
            self.adj[M2].append(M1)
        
        # AGGIORNAMENTO MACCHINE
        devCat = requests.get(self.srvCat+'get/serviceID?serviceID=Device+Catalog').json()['url']
        for machine in [M1,M2]:
            payload = dict(topics=[]) 
            for elem in self.adj[machine]:
                payload['topics'].append(f"IOT/{gym}/{room}/MACHINE/{elem}/STATUS") 
            mUrl = requests.get(f"{devCat}get/deviceID?deviceID={machine}").json()['address']
            requests.put(mUrl+'configuration',data=json.dumps(payload))
    
    def sortData(self):
        for gym in self.dataStructure:
            self.dataStructure = {k: v for k, v in sorted(self.dataStructure.items(), key=lambda item: item[0])}
        for gym in self.dataStructure:
            for room in self.dataStructure[gym]:
                    self.dataStructure[gym] = {k: v for k, v in sorted(self.dataStructure[gym].items(), key=lambda item: item[0])}
        for gym in self.dataStructure:
            for room in self.dataStructure[gym]:
                self.dataStructure[gym][room] = {k: v for k, v in sorted(self.dataStructure[gym][room].items(), key=lambda item: item[0])}
                for idx,dev in enumerate(self.dataStructure[gym][room]):
                    self.dataStructure[gym][room][dev]['idx'] = idx 

    def updateStatus(self,activeDevices):
        freshSet = set()
        if activeDevices == {}: return
        freshSet.update([device['deviceID'] for device in activeDevices])
        ## replace with compact form
        for gym in self.dataStructure:
            for room in self.dataStructure[gym]:
                self.totalSet.update(list(self.dataStructure[gym][room]))
        # ############################################
        unavailableDev = self.totalSet.difference(freshSet)      
        devToUpdate = self.oldSet^freshSet

        self.oldSet = freshSet

        if len(devToUpdate) == 0:return
        self.updateFlag['status'] = True

        for mod in devToUpdate:
            for gym in self.dataStructure:
                for room in self.dataStructure[gym]:
                    retVal = self.dataStructure[gym][room].get(mod,None)
                    if retVal!= None:
                        if mod in unavailableDev: statusVal = 3
                        else: statusVal = 0
                        self.dataStructure[gym][room][mod]['status'] = statusVal
                        self.status[gym]['status'] = statusVal
                        self.status[gym][room]['status'] = statusVal
                        self.updateList.append(f"{gym}///{statusVal}")
                        self.updateList.append(f"{gym}/{room}//{statusVal}")
                        self.updateList.append(f"{gym}/{room}/{mod}/{statusVal}")
   
    def updateData(self):
        # Retrieve active devices from device catalog
        try:
            devCat = requests.get(self.srvCat+'get/serviceID?serviceID=Device+Catalog').json()['url']
            activeDevices = requests.get(devCat+'get/deviceID?').json()
        except:
            self.connStatus.emit({"status":"red","description":"Connection Error"})
            return

        self.connStatus.emit({"status":"green","description":"Connected"})
        self.updateStatus(activeDevices)

        for device in activeDevices:
            dGym = device.get('gymName',None)
            dRoom = device.get('roomName',None)
            dID = device.get('deviceID',None)

            if device.get('isMachine',None) == None: continue
            
            if dID not in list(self.machines) and device.get('isMachine',None) != True: 
                topic = f"IOT/{dGym}/{dRoom}/DEVICE/{dID}/INFO"
                self.machines[dID] = topic
                self.subscribeTo(topic)

            gymVal = self.dataStructure.get(dGym,None)
            if gymVal == None:
                self.dataStructure[dGym] = {dRoom:{dID:dict(type=device['type'],status=0,idx=None,description=device['description'],shordDesc=device['shortDesc'],isMachine=device['isMachine'])}}
                self.status[dGym] = {dRoom:dict(status=0),'status':0}
                self.updateFlag['gyms'] = True
                self.updateFlag['rooms'] = True
                self.updateFlag['devices'] = True
                self.updateList.append(f"{dGym}/{dRoom}/{dID}/x")
                continue
            
            roomVal = self.dataStructure[dGym].get(dRoom,None)
            if roomVal == None: 
                self.dataStructure[dGym][dRoom] = {dID:dict(type=device['type'],status=0,idx=None,description=device['description'],shordDesc=device['shortDesc'],isMachine=device['isMachine'])}
                self.status[dGym][dRoom] = dict(status=0)
                self.updateFlag['rooms'] = True
                self.updateList.append(f"{dGym}/{dRoom}/{dID}/x")
                continue

            devVal = self.dataStructure[dGym][dRoom].get(dID,None)
            if devVal == None: 
                self.dataStructure[dGym][dRoom][dID] = dict(type=device['type'],status=0,idx=None,description=device['description'],shordDesc=device['shortDesc'],isMachine=device['isMachine'])
                self.updateFlag['devices'] = True
                self.updateList.append(f"{dGym}/{dRoom}/{dID}/x")
                continue

            if self.dataStructure[dGym][dRoom][dID]['status'] == 3: #If device was previously unavailable set as available
                self.dataStructure[dGym][dRoom][dID]['status'] = 0
                self.updateFlag['status'] = True
                self.updateList.append(f"{dGym}/{dRoom}/{dID}/0")

        self.sortData()
        if True in self.updateFlag.values(): self.display()
            
    def display(self):
        self.displayLock.acquire()

        if self.context['menu'] == 'home' and True in self.updateFlag.values():
            
            if len(self.updateList) == 0:
                g = self.context['gym']
                r = self.context['room']
                if self.updateFlag['gyms'] == True: self.gymSig.emit([{'name':item,'status':colConv(self.status[item]['status'])} for item in self.dataStructure])
                if self.updateFlag['rooms'] == True: self.roomSig.emit([{'name':item,'status':colConv(self.status[g][item]['status'])} for item in self.dataStructure[g]])
                if self.updateFlag['devices'] == True: self.devSig.emit([{'type':item,'status':colConv(self.dataStructure[g][r][item]['status'])} for item in self.dataStructure[g][r]])

            if self.updateFlag['gyms'] == True:
                self.gymSig.emit([{'name':item,'status':colConv(self.status[item]['status'])} for item in self.dataStructure])
                self.updateFlag['gyms'] = False
           
            if self.updateFlag['status'] == True:
                base = self.updateList.copy()
                for elem in base:
                    g,r,d,s = elem.split('/')

                    if d == '' and r == '': 
                        self.updElem.emit({'model':1,'idx':list(self.dataStructure).index(g),'data':{'status':colConv(s)}})
                    elif d == ''and self.context['gym'] == g: self.updElem.emit({'model':2,'idx':list(self.dataStructure[g]).index(r),'data':{'status':colConv(s)}})
                    elif self.context['room'] == r : self.updElem.emit({'model':3,'idx':list(self.dataStructure[g][r]).index(d),'data':{'status':colConv(s)}})
                    idx = self.updateList.index(elem)
                    self.updateList.pop(idx)
                    self.updateFlag['status'] = False

            if self.updateFlag['rooms'] == True:
                base = self.updateList.copy()
                for elem in base:
                    g,r,d,s = elem.split('/')

                    if self.context['gym'] == g:
                        self.roomSig.emit([{'name':item,'status':colConv(self.status[g][item]['status'])} for item in self.dataStructure[g]])
                        if self.context['room'] == r:
                            self.devSig.emit([{'type':item,'status':colConv(self.dataStructure[g][r][item]['status'])} for item in self.dataStructure[g][r]])
                            idx = self.updateList.index(elem)
                            self.updateList.pop(idx)

            if self.updateFlag['devices'] == True:
                base = self.updateList.copy()
                for elem in base:
                    g,r,d,s = elem.split('/')

                    if self.context['room'] == r:
                        self.devSig.emit([{'type':item,'status':colConv(self.dataStructure[g][r][item]['status'])} for item in self.dataStructure[g][r]])
                        idx = self.updateList.index(elem)
                        self.updateList.pop(idx)
            

            if len(self.updateList) == 0:
                self.updateFlag['rooms'] = False
                self.updateFlag['devices'] = False

        self.displayLock.release()

    def dataStartup(self):    
        try:
            fData = open(self.fileName)
            jData = json.load(fData)
            fData.close()
        except:
            jData = {}
            
        self.oldSet = set(jData.get('oldSet',[]))
        self.dataStructure = jData.get('dataStructure',{})
        self.adj = jData.get('adj',{})
        self.machines = jData.get('machines',{})
        self.status = jData.get('status',{})
        if self.status == {}:
            for gym in self.dataStructure:
                self.status[gym] = dict(status=0)
                for room in self.dataStructure[gym]:
                    self.status[gym][room] = dict(status=0)
        
        for elem in self.machines.values():
            self.subscribeTo(elem)
        
        if len(self.dataStructure)>0: self.updateFlag['gyms'] = True
  
    def saveData(self,toSave):
        self.dataLock.acquire()
        command = dict(dataStructure=self.dataStructure,oldSet=list(self.oldSet),machines=self.machines,adj=self.adj,status=self.status)
        data = {}

        if Path(self.fileName).is_file() == False:
            with open(self.fileName, 'w') as fp:
                json.dump({},fp)
        
        with open(self.fileName) as fp:
            data = json.load(fp)
        
        with open(self.fileName,'w') as fp:
            data[toSave] = command[toSave]
            json.dump(data,fp)

        self.dataLock.release()
    @Slot()
    def goodBye(self):
        print('unsubscribing...')
        for elem in self.machines.values():
            self.mqttClient.unsubscribe(elem)
        for elem in ['oldSet','machines','status','dataStructure','adj']:
            self.saveData(elem)
        print('Finish!')
        self.closingEvent.clear()

class infoDisp():
    def __init__(self,windowObj):
        self.windowObj = windowObj

    exposed = True
    def GET(self, *uri, **params):
        if len(uri) == 0: return json.dumps({"error":"no command found"})
        if uri[0] == 'machineConfiguration':
            deviceID = params.get('deviceID',None)
            machineTopic = params.get('machineTopic',None)
            if deviceID == None or machineTopic == None: return json.dumps({"error":"deviceID not present"})
            
            self.windowObj.machines[deviceID] = machineTopic+'/INFO'
            self.windowObj.subscribeTo(machineTopic+'/INFO')
            self.windowObj.saveData('machines')
            adjList = self.windowObj.adj.get(deviceID,None)
            if adjList == None:  return json.dumps({"error":"deviceID not present in adjacency list"})
            returnD = dict(topics=[])
            for adjMachine in adjList:
                returnD['topics'].append(f"{self.windowObj.machines[adjMachine]}/STATUS")
            print(returnD)
            return json.dumps(returnD)
        return 'Bad Request'
    

if __name__ == "__main__":

    socket_port = 50095
    
    app = QGuiApplication(sys.argv)
    app.setOrganizationName('pythonProj')
    app.setOrganizationDomain('OD')
    srvCatReg = refreshThread('srvCatReg')
    
    # Get Context
    main = MainWindow()
    main.start_engine()
    srvCatReg.start()

    # Cherrypy configuration
    conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tool.session.on': True 
		}
	}
    cherrypy.config.update({'server.socket_host': localIp,'server.socket_port':socketPort})
    cherrypy.tree.mount(infoDisp(main),'/', conf)

    # Set Context
    main.engine.rootContext().setContextProperty("backend", main)
    
    # Load QML file
    main.engine.load(os.fspath(Path(__file__).resolve().parent / "qml/main.qml"))
    
    cherrypy.engine.start()
    if not main.engine.rootObjects():
        sys.exit(-1)
    
    
    sys.exit(app.exec_())