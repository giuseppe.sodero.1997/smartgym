from myMQTT import *
import time
import json
import threading
import cherrypy
import requests
import socket
import binascii

import RPi.GPIO as GPIO
from pn532pi import Pn532, pn532
from pn532pi import Pn532I2c

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


cherrypy.config.update({'server.socket_port': 50041})
class CustomError(Exception):
	def __init__(self, message):
		Exception.__init__(self)
		self.message = message

class Publisher_subscriber():
    exposed = True
    def __init__(self, clientID, broker, port,serviceCatalogUrl):
        self.client=MyMQTT(clientID, broker, port,self)
        self.port=port
        self.broker=broker
        self.clientID=clientID
        self.deviceListLock = threading.Lock()
        self.deviceList = []
        self.serviceCatalogUrl = serviceCatalogUrl
        
    def notify(self,topic,msg):
        r = json.loads(msg)
        for i in range(len(self.deviceList)):
            if self.deviceList[i]['topic'] == topic and self.deviceList['i']['type'] == 'slave':
                self.deviceList[i]['status'] == bool(msg)
                break
        
    def addDev(self,dat):
        data = json.loads(dat)
        try:
            for i in range(len(self.deviceList)): 
                if self.deviceList[i]['deviceID'] == data['deviceID']:
                    raise CustomError
        except CustomError:
            print('deviceID not available\n')
            return
        
        #entering critical section
        self.deviceListLock.acquire()
        self.deviceList.append(data)
        self.deviceListLock.release()
        #exiting critical section
    
    def removeDev(self, data):
        for i in range(len(self.deviceList)):
            if self.deviceList[i]['deviceID'] == data['deviceID']:
                #entering critical section
                self.deviceListLock.acquire()
                self.deviceList.pop(i)
                if(data['type']=='slave'):
                    self.client.unsubscribe(data['topic'])
                self.deviceListLock.release()
                #exiting critical section  
                break
    
    def dataIncoming(self,dat):
        #FORMAT: {"bn":"deviceID", "e": [{"value":"data", "time":"1234567"}]}
        #print(dat)
        data = json.loads(dat)
        for i in range(len(self.deviceList)):
            if self.deviceList[i]['deviceID'] == data['bn']:
                self.client.myPublish(self.deviceList[i]['topic'], data)
                break
            
    def PUT(self, *uri, **param):
        payload = json.loads(cherrypy.request.body.read())
        command = ''      
        
        r = list(uri)
        if len(r)>0: command = r[0]
        deviceID = payload.get('deviceID',None)
        val = item.get('value',None)

        if command == 'switch':
            #deviceID = payload.get('deviceID',None)

            devCatUrl = requests.get(self.serviceCatalogUrl+'get/serviceID?serviceID=Device+Catalog').json()['url']
            devData = requests.get(devCatUrl + 'get/deviceID?deviceID='+deviceID).json()
            
            gymName = devData.get('gymName',None)
            roomName = devData.get('roomName',None)

            if payload.get('value',None) == True : value = 0
            else: value = 2

            if gymName != None and roomName != None:
                topic = f"IOT/{gymName}/{roomName}/DEVICE/{deviceID}/INFO"
                self.client.myPublish(topic, value)
        
        item = next((item for item in self.client.deviceList if item['deviceID'] == deviceID),None)
        if val == 1: sel = item['pinOut'].get('onPin')
        else: sel = item['pinOut'].get('offPin')
        
        GPIO.output(sel,GPIO.HIGH)
        time.sleep(3)
        GPIO.output(sel,GPIO.LOW)
        
        with open('dataBus.txt', 'a+') as fp:
            json.dump(payload, fp, indent = 4)

    
    def start(self):
        self.client.start()
    def stop(self):
        self.client.stop()
    
class RegistrationThread(threading.Thread):
    def __init__(self, serviceCatalogUrl, client):
        threading.Thread.__init__(self)
        self.serviceCatalogUrl = serviceCatalogUrl
        self.deviceClient = client
    def run(self):
        while True:
            time.sleep(10)
            #find device Catalog
            r = requests.get(self.serviceCatalogUrl+'get/serviceID?serviceID=Device+Catalog')
            #print(f'{r.text}')
            deviceCatalog = json.loads(r.text)
            #enter critical section
            self.deviceClient.deviceListLock.acquire()

            for i in range(len(self.deviceClient.deviceList)):
                r = requests.put(deviceCatalog['url']+'add/deviceID', data = json.dumps(self.deviceClient.deviceList[i]))

            self.deviceClient.deviceListLock.release()
            #exit critical section
class inputThread(threading.Thread):
   
    def __init__(self, client):
        threading.Thread.__init__(self)
        self.client = client
        PN532_I2C = Pn532I2c(1)
        self.nfc = Pn532(PN532_I2C)
        self.nfc.begin()
        versiondata = self.nfc.getFirmwareVersion()
        if (not versiondata):
            print("Didn't find PN53x board")
            raise RuntimeError("Didn't find PN53x board")  # halt
        self.nfc.SAMConfig()

    def devInit(self):
        with open('conf.txt') as fp:
            #l = fp.readlines()
            for line in fp:
                data = json.loads(line.split('\n')[0])
                pinData = data.get('pinOut',None)
                if pinData != None:
                    for pin in pinData.values():
                        GPIO.setup(pin,GPIO.OUT)
                        GPIO.output(pin,GPIO.LOW)
                        print(pin)
                
                self.client.addDev(json.dumps(data))

        
    def run(self):
        self.devInit()
        while True:
            success, uid = self.nfc.readPassiveTargetID(pn532.PN532_MIFARE_ISO14443A_106KBPS)
            if (success):
                localDeviceID = 'RFIDdoor1'
                item = next((item for item in self.client.deviceList if item['deviceID'] == localDeviceID),None)
                if item != None: 
                    localDoorID = item.get('doorID',None)
                    _from = item.get('from',"")
                    uidDec = binascii.hexlify(uid).decode('utf-8')
                    data = {"RFIDdata":uidDec,"doorID":localDoorID,"from":_from}
                    self.client.dataIncoming(json.dumps(dict(bn=localDeviceID,e=[data])))
                    #FORMAT: {"bn":"deviceID", "e": [{"value":"data", "time":"1234567"}]}

if __name__=='__main__':
    
    global localIp
    global socketPort
    s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    s.connect(("8.8.8.8",80))
    localIp = s.getsockname()[0]
    socketPort = 9091
    serviceCatalogUrl = 'http://192.168.1.9:8080/'

    r = requests.get(serviceCatalogUrl+'get/broker')
    config = json.loads(r.text)
    client = Publisher_subscriber( 'client1', config['broker'], config['port'],serviceCatalogUrl)
    thread1 = RegistrationThread(serviceCatalogUrl, client)
    thread2 = inputThread(client)
    conf = {
		'/':{
				'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
				'tools.sessions.on': True
		}
	}
    cherrypy.config.update({'server.socket_host': localIp,'server.socket_port':socketPort})
    cherrypy.tree.mount(client,'/', conf)
    cherrypy.engine.start()
    thread1.start()
    thread2.start()
    client.start()
    cherrypy.engine.block()


