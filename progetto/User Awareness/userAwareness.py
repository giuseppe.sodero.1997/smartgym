from myMQTT import *
import time
import json
import threading
import cherrypy
import requests
import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
class TelegramBot():
    def __init__(self, clientID, serviceCatalogUrl,token):
        r = requests.get(serviceCatalogUrl+'get/broker')
        brokerData = json.loads(r.text)
        self.tokenBot = token
        self.bot = telepot.Bot(self.tokenBot)
        self.machineDict = {}
        self.roomDict = {}
        MessageLoop(self.bot, {'chat':self.on_chat_message, 'callback_query': self.on_callback_query}).run_as_thread()
        self.clientID = clientID
        self.broker = brokerData['broker']
        self.port = brokerData['port']
        self.client = MyMQTT(clientID, self.broker, self.port,self)
        self.queryData = {
            'queryType':None,
            'queryID':None
        }
    def start(self):
        self.client.start()
        self.client.mySubscribe('IOT/+/+/info')
        self.client.mySubscribe('IOT/+/+/+/+/info')
    def notify(self,topic,msg):
        msg = json.loads(msg)
        print(f'topic is: {topic}, msg = {msg}')
        msgTopicList = topic.split("/")
        if len(msgTopicList) == 4:
           self.roomDict[topic] = json.dumps(msg)
           print(self.roomDict)
        elif len(msgTopicList) == 6:
            self.machineDict[topic] = json.dumps(msg)
            print(self.machineDict)
        else:
            print('topic Errato')
        
    def stop(self):
        self.client.stop
    def on_chat_message(self,msg):
        content_type, chat_type, chat_ID = telepot.glance(msg)
        print(msg['text'])
        if msg['text'] == '/gyms':
            gymList = []
            roomTopics = self.roomDict.keys()
            for topic in roomTopics:
                gym = topic.split('/')[1]
                if gym not in gymList:
                    gymList.append(gym)
            buttons = [[]]
            for gym in gymList:
                callbackData = self.queryData.copy()
                callbackData['queryID'] = gym
                callbackData['queryType'] = 'gym'
                buttons[0].append(InlineKeyboardButton(text = gym , callback_data =json.dumps(callbackData)))
            keyboard = InlineKeyboardMarkup(inline_keyboard = buttons)
            self.bot.sendMessage(chat_ID, text = 'choose gym', reply_markup=keyboard)
        else:
            self.bot.sendMessage(chat_ID, text = 'wrong command')
    
    
    
    def on_callback_query(self, msg):
        query_ID, chat_ID, query_data = telepot.glance(msg, flavor = 'callback_query')
        query_data = json.loads(query_data)
        print(query_data)
        if query_data.get('queryType', None)=='gym':
            roomList = []
            roomTopics = self.roomDict.keys()
            for topic in roomTopics:
                room = topic.split('/')[2]
                if room not in roomList and topic.split('/')[1] == query_data['queryID']:
                    roomList.append(room)
            buttons = [[]]
            for room in roomList:
                callbackData = self.queryData.copy()
                callbackData['queryID'] = room
                callbackData['queryType'] = 'room'
                buttons[0].append(InlineKeyboardButton(text = room , callback_data = json.dumps(callbackData)))
            keyboard = InlineKeyboardMarkup(inline_keyboard = buttons)
            self.bot.sendMessage(chat_ID, text = 'choose room', reply_markup=keyboard)
            
        elif query_data.get('queryType', None)=='room':
            machineList = []
            machineTopics = self.machineDict.keys()
            for topic in machineTopics:
                machine = topic.split('/')[4]
                if machine not in machineList and topic.split('/')[2] == query_data['queryID']:
                    machineList.append(machine)
            buttons = [[]]
            for machine in machineList:
                callbackData = self.queryData.copy()
                callbackData['queryID'] = machine
                callbackData['queryType'] = 'machine'
                buttons[0].append(InlineKeyboardButton(text = machine , callback_data = json.dumps(callbackData)))
            keyboard = InlineKeyboardMarkup(inline_keyboard = buttons)
            print(query_data)
            self.bot.sendMessage(chat_ID, text = 'choose machine', reply_markup=keyboard)
        elif query_data.get('queryType', None)=='machine':
            for machineTopic in self.machineDict.keys():
                if  query_data.get('queryID', None) == machineTopic.split('/')[4]:
                    print(machineTopic)
                    self.bot.sendMessage(chat_ID, text = 'machine status:\n\n'+ self.machineDict[machineTopic])   
if __name__ == '__main__':
    bot = TelegramBot('userAwareness', 'http://127.0.0.1:8080/','5005021965:AAEmzdKXXKRCeJ_ve6ZDBB1EsXFUr-EAJrU')
    bot.start()
    while True:
        pass
    

