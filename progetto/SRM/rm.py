from apscheduler.schedulers.background import BackgroundScheduler
import threading
from MyMQTT import *
import time
import json
from datetime import datetime, timezone
import requests


class roomManager(threading.Thread):
    def __init__(self,roomInfo):
        self.srvCat = roomInfo['url_srvCat']
        self.roomInfo = roomInfo
        self.gymName = self.roomInfo['gymName']
        self.roomName = self.roomInfo['roomName']
        self.topic = 'IOT/' + self.gymName + '/' + self.roomName + '/INFO'
        self.inRoom = {"0":0,"1":0,"2":0,"3":0}
        self.clientID = f'{self.gymName}/{self.roomName}'
        self.totalThreshold = roomInfo['totalThreshold']
        
        self.scheduler = BackgroundScheduler(deamon=True,timezone='Europe/Rome')
        self.actionLock = threading.Lock()
        self.notifyLock = threading.Lock()

        config = json.loads((requests.get(self.srvCat+'get/broker')).text)
        self.broker = config['broker']
        self.port = config['port']
        self.client = MyMQTT(self.clientID,self.broker,self.port,self)
    
    def start(self):
        self.client.start()
        self.client.mySubscribe(self.topic)
        self.scheduledEvents()
        # for elem in ['TV','Audio','Spinbike','Tapis','Light','Ellittica','Cyclette']:
        #     self.executeAction(elem,False)

    def stop(self):
        self.client.stop()

    def notify(self,topic,payload): 
        # print(f"\n\n\n{topic }--> {payload}\n")

        payload = json.loads(payload)

        userType = payload['userType'].split('type')[1]
        isEntering = payload['isEntering']
        
        
        for elem in range(4):
            self.inRoom[str(elem)] = payload['type'+str(elem)]

        if not isEntering:
            if self.inRoom[userType] == 0 and self.inProgress != True:  
                inSet = set()  
                for usr in self.inRoom:
                    if self.inRoom[usr] > 0 :inSet.update(self.roomInfo['actionDict'][usr])
                for item in set(self.roomInfo['actionDict'][userType]) - inSet:
                    self.executeAction(item,False)
                return
        
        for type in self.roomInfo['actionDict'][userType]:
            self.executeAction(type,self.roomInfo['actionDict'][userType][type])

    def scheduledEvents(self):
        self.scheduler.start()
        for runDate in self.roomInfo['scheduledDict']:
            for device in self.roomInfo['scheduledDict'][runDate]:
                print(f"------>{runDate}<-------")
                self.scheduler.add_job(self.executeAction,'date',run_date = runDate,args=(device,self.roomInfo['scheduledDict'][runDate][device]))

    def executeAction(self,type,action):
        self.actionLock.acquire() 
        # Update inRoom flag option
        if type == 'Start':
            if action == True: self.inProgress = True
            else:   
                self.inProgress = False
            return

        # Update threshold value through entry control
        if type == 'Threshold':
            entryControlUrl = requests.get(self.srvCat+'get/serviceID?serviceID=entry_control').json()['url']
            payload = dict(roomTopic = f'IOT/{self.gymName}/{self.roomName}', userThreshold=int(action), totalThreshold=self.totalThreshold)
            response = requests.post(entryControlUrl+'setThreshold', data=json.dumps(payload))
            return

        # Retrieve device catalog from resource catalog
        try:
            devCat = requests.get(self.srvCat+'get/serviceID?serviceID=Device+Catalog').json()['url']
        except:
            print('Resource catalog not reachable')
        
        try:
            activeDevices = requests.get(devCat+'get/deviceID').json()
        except:
            print('Device catalog not reachable')
        
        # Filter active devices and send the action
        for device in activeDevices:
            if device.get('gymName') == self.gymName and device.get('roomName') == self.roomName and device.get('type') == type:
                payload = dict(deviceID = device['deviceID'], value = action)
                try:
                    print(payload) 
                    response = requests.put(device['address']+'switch',json.dumps(payload))
                except:
                    print('Something gone wrong')
        self.actionLock.release()

if __name__ == '__main__':    


    #Inizializzazione
    roomInfo = json.load(open('roomInfo copy.json'))
    
    s1 = threading.Event()
    s1.clear()
    room1 = roomManager(s1,roomInfo)

    room1.start()

    time.sleep(300)
    room1.stop()



