from MyMQTT import *
import json
import time
import random

class current_user_pub():
    def __init__(self,clientID,topic,broker,port):
        self.client=MyMQTT(clientID,broker,port,None)
        self.topic=topic
        self.single={
            'bn': topic,
			'users':[
					{
						'ID': 'personal_ID',
						'Type': "role",
                        'Last': 0 
					}
				]
			}
        
        
    def start(self):
        self.client.start()
        
    def stop(self):
        self.client.stop()
        
    def publish(self):
        single = self.single
        
        single["users"][0]["ID"] = input("ID-->")
        if len(single["users"][0]["ID"])>5:
            single["users"][0]["Type"] = "User"
        elif single["users"][0]["ID"][2] == "w":
            single["users"][0]["Type"] = "Worker"
        elif single["users"][0]["ID"][2] == "o":
            single["users"][0]["Type"] = "Owner"
       
        #json.dump(single,open("current_user.json","w"))
        self.client.myPublish(self.topic,single)
        print(json.dumps(single,indent=4))
        print("---PUBLISHED---")
        
        
class access_response_client():
    def __init__(self,clientID,topic,broker, port):
        self.client=MyMQTT(clientID,broker,port,self)
        self.topic=topic
        
        
    def start(self):
        self.client.start()
        self.client.mySubscribe(self.topic)
        
    def stop(self):
        self.client.stop()
        
    def notify(self,topic,msg):
        status = json.loads(msg)
        status = int(status)
        if status == 0:
            print("ACCESS DENIED")
        else: print("---WELCOME---")
        
     
if __name__ == "__main__":
    conf=json.load(open("settings.json"))
    broker=conf["broker"]
    port=conf["port"]
    CUP = current_user_pub("user_current_publisher","IoT/Smart_Gym/user_list",broker,port)
    ARC = access_response_client("door_status_subscriber","IoT/Smart_Gym/door_status",broker,port)
    CUP.client.start()
    
    while True:
        CUP.publish()
        ARC.start()
        time.sleep(3)
        ARC.stop()
    CUP.client.stop()
    
 
 

        
            