from MyMQTT import *
import json
import time
import random

class door_status():
    def __init__(self,clientID,topic,broker,port):
        self.client=MyMQTT(clientID,broker,port,None)
        self.topic=topic
        
        
    def start(self):
        self.client.start()
        
    def stop(self):
        self.client.stop()
    
    def publish(self,max):
        self.__message=json.load(open("users.json"))
        message = self.__message
        last = 0
        if message == {}:
            counter = 0
        else:
            counter = 0
            for i in range(len(message["users"])):
                if message["users"][i] != {}:
                    counter+=1
                    if message["users"][i]["Last"] ==1:
                        last = 1
                    
        
                    
        if counter >= max and last == 0:
            status = 0
        else: status = 1
                    
        
                    
        print("------")
        print(f"Counter: {counter}")
        print(f"Status: {status}")
        print("------")
        self.client.myPublish(self.topic,status)
        print("---PUBLISHED---")
        
class current_user_client():
    def __init__(self,clientID,topic,broker, port,max):
        self.client=MyMQTT(clientID,broker,port,self)
        self.topic=topic
        self.max = max
        
    def start(self):
        self.client.start()
        self.client.mySubscribe(self.topic)
        
    def stop(self):
        self.client.stop()
        
    def notify(self,topic,msg):
        payload=json.loads(msg)
        self.data = json.load(open("users.json"))
        user = self.data
        if user == {}:
            counter = 0
        else:
            counter = 0
            for i in range(len(user["users"])):
                if user["users"][i] != {}:
                    counter+=1
                    user["users"][i]["Last"] = 0
                    print(f"SONO QUA---{i}")
                    print(user["users"][i]["Last"])
                    
                    
        if self.data == {}:
            self.data = payload
        else:
            i=0
            delete = 0
            while i<len(self.data["users"]) and delete == 0:
                if  self.data["users"][i] != {}  and  self.data["users"][i]["ID"] == payload["users"][0]["ID"]:
                    self.data["users"][i].clear()
                    delete = 1
                i+=1
            if delete==0 and counter<self.max:
                i = 0
                empty = 0
                while i<len(self.data["users"]) and empty == 0:
                    if self.data["users"][i] == {}:
                        self.data["users"][i] = payload["users"][0]
                        if counter == self.max-1:
                            self.data["users"][i]["Last"] = 1
                        empty = 1
                    i +=1
                if empty == 0:
                    self.data["users"].append(payload["users"][0])
                    if counter == self.max-1:
                        self.data["users"][i]["Last"] = 1
        
        print(self.data)
        json.dump(self.data,open("users.json","w"))
        
                
if __name__ == "__main__":
    
    conf=json.load(open("settings.json"))
    broker=conf["broker"]
    port=conf["port"]
    cap = int(5)
    CUC=current_user_client("user_current_subscriber","IoT/Smart_Gym/user_list",broker,port,cap)
    DS=door_status("door_status_publisher","IoT/Smart_Gym/door_status",broker,port)
    DS.client.start()
    print("---DOOR CONTROL STARTED---")
    while True:
        DS.publish(cap)
        CUC.start()
        time.sleep(1)
        CUC.stop()
    DS.client.stop()
    
        
    
    